<?php

use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Session */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="session-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'filmID')->dropDownList($model->filmsList) ?>

	<?= $form->field($model, 'ticketID')->dropDownList($model->ticketsList) ?>

	<?= $form->field($model, 'date')->widget(DatePicker::className(), [
		'name' => 'check_issue_date',
		'convertFormat' => true,
		'options' => ['placeholder' => 'Select issue date ...'],
		'pluginOptions' => [
			'format' => 'php:Y-m-d',
			'todayHighlight' => true
		]
	]) ?>

	<?= $form->field($model, 'time_session')->widget(TimePicker::className(), [
		'name' => 'start_time',
		'value' => '11:24 AM',
		'pluginOptions' => [
			'showSeconds' => true
		]
	]) ?>

	<?= $form->field($model, 'status_session')->dropDownList($model->statusSession) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
