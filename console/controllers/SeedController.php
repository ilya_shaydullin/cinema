<?php

namespace console\controllers;

use common\models\User;
use yii\console\Controller;

class SeedController extends Controller
{

	public function actionIndex()
	{
		$admin = new User([
			'username' => 'admin',
			'password' => 'admin',
			'status' => User::STATUS_ACTIVE,
		]);

		$admin->save();

		echo 'Great! Admin is created!';
	}
}