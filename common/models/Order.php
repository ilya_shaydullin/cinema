<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $orderID
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $date
 * @property integer $phone
 * @property integer $sessionID
 *
 * @property Session $session
 */
class Order extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%order}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['first_name', 'last_name', 'email', 'phone', 'sessionID'], 'required'],
			[['first_name', 'last_name', 'email'], 'string'],
			[['email'], 'email'],
			[['date'], 'safe'],
			[['phone', 'sessionID'], 'integer'],
			[['sessionID'], 'exist', 'skipOnError' => true, 'targetClass' => Session::className(), 'targetAttribute' => ['sessionID' => 'sessionID']],
		];
	}


	public function beforeSave($insert)
	{
		if(parent::beforeSave($insert)){
			$this->date = Date('Y-m-d');
			return true;
		}else{
			return false;
		}

	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'orderID' => Yii::t('app', 'Order ID'),
			'first_name' => Yii::t('app', 'First Name'),
			'last_name' => Yii::t('app', 'Last Name'),
			'email' => Yii::t('app', 'Email'),
			'date' => Yii::t('app', 'Date'),
			'phone' => Yii::t('app', 'Phone'),
			'sessionID' => Yii::t('app', 'Session ID'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSession()
	{
		return $this->hasOne(Session::className(), ['sessionID' => 'sessionID']);
	}
}
