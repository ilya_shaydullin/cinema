<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Session;

/**
 * SessionSearch represents the model behind the search form about `common\models\Session`.
 */
class SessionSearch extends Session
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sessionID', 'filmID', 'ticketID', 'status_session'], 'integer'],
            [['date', 'time_session'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type = 'back')
    {

        $query = ( 'back' == $type) ? Session::find() : Session::find()->where(['status_session' => 1]) ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sessionID' => $this->sessionID,
            'filmID' => $this->filmID,
            'ticketID' => $this->ticketID,
            'date' => $this->date,
            'time_session' => $this->time_session,
            'status_session' => $this->status_session,
        ]);

        return $dataProvider;
    }
}
