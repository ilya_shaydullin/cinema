<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%film}}".
 *
 * @property integer $filmID
 * @property string $name_film
 * @property string $genre
 * @property string $year
 * @property integer $duration
 * @property integer $status_film
 *
 * @property Session[] $sessions
 */
class Film extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%film}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name_film', 'genre', 'year', 'duration', 'status_film'], 'required'],
			[['name_film', 'genre'], 'string'],
			[['year'], 'safe'],
			[['duration', 'status_film'], 'integer'],
			[['status_film'], 'default', 'value' => 1]
		];
	}

	public function getStatusFilm()
	{
		return [
			1 => 'Активно',
			0 => 'Скрыто',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'filmID' => Yii::t('app', 'Film ID'),
			'name_film' => Yii::t('app', 'Name Film'),
			'genre' => Yii::t('app', 'Genre'),
			'year' => Yii::t('app', 'Year'),
			'duration' => Yii::t('app', 'Duration'),
			'status_film' => Yii::t('app', 'Status Film'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSessions()
	{
		return $this->hasMany(Session::className(), ['filmID' => 'filmID']);
	}
}
