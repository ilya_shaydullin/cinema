<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%ticket}}".
 *
 * @property integer $ticketID
 * @property integer $price
 * @property integer $hall
 * @property integer $line
 * @property integer $place
 * @property integer $status_ticket
 *
 * @property Session[] $sessions
 */
class Ticket extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%ticket}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['price', 'hall', 'line', 'place', 'status_ticket'], 'required'],
			[['price', 'hall', 'line', 'place', 'status_ticket'], 'integer'],
			[['status_ticket'], 'default', 'value' => 1]
		];
	}

	public function getStatusTicket()
	{
		return [
			1 => 'Активно',
			0 => 'Скрыто',
		];
	}

	public function getName()
	{
		return 'Билет №' . $this->ticketID . ' Ряд:' . $this->hall . ' Место:' . $this->place . ' Цена:' . $this->place;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'ticketID' => Yii::t('app', 'Ticket ID'),
			'price' => Yii::t('app', 'Price'),
			'hall' => Yii::t('app', 'Hall'),
			'line' => Yii::t('app', 'Line'),
			'place' => Yii::t('app', 'Place'),
			'status_ticket' => Yii::t('app', 'Status Ticket'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSessions()
	{
		return $this->hasMany(Session::className(), ['ticketID' => 'ticketID']);
	}
}
