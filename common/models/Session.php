<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%session}}".
 *
 * @property integer $sessionID
 * @property integer $filmID
 * @property integer $ticketID
 * @property string $date
 * @property string $time_session
 * @property integer $status_session
 *
 * @property Order[] $orders
 * @property Film $film
 * @property Ticket $ticket
 */
class Session extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%session}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['filmID', 'ticketID', 'date', 'time_session', 'status_session'], 'required'],
			[['filmID', 'ticketID', 'status_session'], 'integer'],
			[['date', 'time_session'], 'safe'],
			[['filmID'], 'exist', 'skipOnError' => true, 'targetClass' => Film::className(), 'targetAttribute' => ['filmID' => 'filmID']],
			[['ticketID'], 'exist', 'skipOnError' => true, 'targetClass' => Ticket::className(), 'targetAttribute' => ['ticketID' => 'ticketID']],
			[['status_session'], 'default', 'value' => 1]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'sessionID' => Yii::t('app', 'Session ID'),
			'filmID' => Yii::t('app', 'Film ID'),
			'ticketID' => Yii::t('app', 'Ticket ID'),
			'date' => Yii::t('app', 'Date'),
			'time_session' => Yii::t('app', 'Time Session'),
			'status_session' => Yii::t('app', 'Status Session'),

		];
	}

	public function getFilmsList()
	{
		return ArrayHelper::map(Film::find()->where([
			'status_film' => 1
		])->all(), 'filmID', 'name_film');
	}

	public function getTicketsList()
	{
		return ArrayHelper::map(Ticket::find()->where([
			'status_ticket' => 1
		])->all(), 'ticketID', 'name');
	}


	public function getStatusSession()
	{
		return [
			1 => 'Активно',
			0 => 'Скрыто',
		];
	}

	public function getName()
	{
		return $this->film->name_film . ' ' . $this->date . ' ' . $this->time_session;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrders()
	{
		return $this->hasMany(Order::className(), ['sessionID' => 'sessionID']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFilm()
	{
		return $this->hasOne(Film::className(), ['filmID' => 'filmID']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTicket()
	{
		return $this->hasOne(Ticket::className(), ['ticketID' => 'ticketID']);
	}
}
