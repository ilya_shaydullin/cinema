<?php
return [
	'language' => 'ru-RU',
	'sourceLanguage' => 'en-US',
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'components' => [
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'i18n' => [
			'translations' => [
				'app*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@common/messages',
					'fileMap' => [
						'app' => 'app.php',
						'app/auth' => 'auth.php'
					]
				],
			],
		],
	],
];
