<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SessionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sessions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="session-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?php Pjax::begin(); ?>    <?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			[
				'label' => Yii::t('app', 'Film'),
				'value' => function ($model, $key, $index, $column) {
					return $model->film->name_film;
				}
			],
			[
				'label' => Yii::t('app', 'Ticket'),
				'value' => function ($model, $key, $index, $column) {
					return $model->ticket->name;
				}
			],
			[
				'attribute' => 'date',
				'format' => ['date', 'php:Y-m-d']
			],
			'time_session',
			[
				'attribute' => '',
				'value' => function ($model) {
					if ($model->getOrders()->exists()) {
						return 'Заказано';
					} else {
						return Html::a(Html::encode('Заказать'), Url::to(['order/create', 'id' => $model->sessionID]));
					}
				},
				'format' => 'raw',
			],
//			'status_session',

		],
	]); ?>
	<?php Pjax::end(); ?></div>
