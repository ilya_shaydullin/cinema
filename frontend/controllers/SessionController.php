<?php

namespace frontend\controllers;

use Yii;
use common\models\Session;
use common\models\SessionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SessionController implements the CRUD actions for Session model.
 */
class SessionController extends \backend\controllers\SessionController
{

	/**
	 * Lists all Session models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new SessionSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'front');

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

}
