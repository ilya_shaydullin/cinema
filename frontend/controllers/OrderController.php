<?php

namespace frontend\controllers;

use Yii;
use common\models\Order;
use common\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends \backend\controllers\OrderController
{


    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = 0)
    {
        $model = new Order();

        $model->sessionID = (0 == $id) ? '' : $id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['session/index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


}
